CREATE TABLE `spawn_areas_groups` (
	`id` varchar(100) NULL,
	`max_npc_count` TINYINT UNSIGNED DEFAULT 1 NULL,
	`ai` varchar(100) NULL,
	`spawn_areas_id` TEXT NULL,
	CONSTRAINT spawn_areas_groups_pk PRIMARY KEY (id)
)