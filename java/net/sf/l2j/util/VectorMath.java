package net.sf.l2j.util;

import net.sf.l2j.gameserver.model.location.Location;

public class VectorMath
{
    /**
     * AB - line. C - point to project on the AB.
     */
    public static Location getProjection(Location a, Location b, Location c)
    {
        Location d = new Location(0, 0, 0);
        int abx = b.getX() - a.getX();
        int aby = b.getY() - a.getY();
        int dacab = (c.getX() - a.getX()) * abx + (c.getY() - a.getY()) * aby;
        int dab = abx * abx + aby * aby;
        double t = (double) dacab / (double) dab;
        d.setX((int) Math.round(a.getX() + abx * t));
        d.setY((int) Math.round(a.getY() + aby * t));

        int minX = Math.min(a.getX(), b.getX());
        int maxX = Math.max(a.getX(), b.getX());

        int minY = Math.min(a.getY(), b.getY());
        int maxY = Math.max(a.getY(), b.getY());

        if (d.getX() < minX || d.getX() > maxX) return null;
        if (d.getY() < minY || d.getY() > maxY) return null;

        return d;
    }


    // Given three collinear points p, q, r, the function checks if
    // point q lies on line segment 'pr'
    public static boolean onSegment(Location p, Location q, Location r)
    {
        return q.getX() <= Math.max(p.getX(), r.getX()) && q.getX() >= Math.min(p.getX(), r.getX()) &&
                q.getY() <= Math.max(p.getY(), r.getY()) && q.getY() >= Math.min(p.getY(), r.getY());
    }

    // To find orientation of ordered triplet (p, q, r).
    // The function returns following values
    // 0 --> p, q and r are collinear
    // 1 --> Clockwise
    // 2 --> Counterclockwise
    public static int orientation(Location p, Location q, Location r)
    {
        // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
        // for details of below formula.
        int val = (q.getY() - p.getY()) * (r.getX() - q.getX()) -
                (q.getX() - p.getX()) * (r.getY() - q.getY());

        if (val == 0) return 0; // collinear

        return (val > 0)? 1: 2; // clock or counterclockwise
    }


    // Function that returns true if line segment 'p1q1'
    // and 'p2q2' intersect.
    public static boolean doIntersect(Location p1, Location q1, Location p2, Location q2)
    {
        // Find the four orientations needed for general and
        // special cases
        int o1 = orientation(p1, q1, p2);
        int o2 = orientation(p1, q1, q2);
        int o3 = orientation(p2, q2, p1);
        int o4 = orientation(p2, q2, q1);

        // General case
        if (o1 != o2 && o3 != o4)
            return true;

        // Special Cases
        // p1, q1 and p2 are collinear and p2 lies on segment p1q1
        if (o1 == 0 && onSegment(p1, p2, q1)) return true;

        // p1, q1 and q2 are collinear and q2 lies on segment p1q1
        if (o2 == 0 && onSegment(p1, q2, q1)) return true;

        // p2, q2 and p1 are collinear and p1 lies on segment p2q2
        if (o3 == 0 && onSegment(p2, p1, q2)) return true;

        // p2, q2 and q1 are collinear and q1 lies on segment p2q2
        return o4 == 0 && onSegment(p2, q1, q2);// Doesn't fall in any of the above cases
    }


    /**
     * Returns the intersection point of the given lines.
     * Returns Empty if the lines do not intersect.
     * Source: http://mathworld.wolfram.com/Line-LineIntersection.html
     */
    public static Location intersection(Location v1, Location v2, Location v3, Location v4)
    {
        float tolerance = 0.000001f;

        float a = Det2(v1.getX() - v2.getX(), v1.getY() - v2.getY(), v3.getX() - v4.getX(), v3.getY() - v4.getY());
        if (Math.abs(a) < tolerance) return null; // Lines are parallel

        float d1 = Det2(v1.getX(), v1.getY(), v2.getX(), v2.getY());
        float d2 = Det2(v3.getX(), v3.getY(), v4.getX(), v4.getY());
        float x = Det2(d1, v1.getX() - v2.getX(), d2, v3.getX() - v4.getX()) / a;
        float y = Det2(d1, v1.getY() - v2.getY(), d2, v3.getY() - v4.getY()) / a;

        if (x < Math.min(v1.getX(), v2.getX()) - tolerance || x > Math.max(v1.getX(), v2.getX()) + tolerance) return null;
        if (y < Math.min(v1.getY(), v2.getY()) - tolerance || y > Math.max(v1.getY(), v2.getY()) + tolerance) return null;
        if (x < Math.min(v3.getX(), v4.getX()) - tolerance || x > Math.max(v3.getX(), v4.getX()) + tolerance) return null;
        if (y < Math.min(v3.getY(), v4.getY()) - tolerance || y > Math.max(v3.getY(), v4.getY()) + tolerance) return null;

        return new Location((int) x, (int) y, v1.getZ());
    }

    /**
     * Returns the determinant of the 2x2 matrix.
     */
    private static float Det2(float x1, float x2, float y1, float y2)
    {
        return (x1 * y2 - y1 * x2);
    }


    public static double distanceSquare(Location a, Location b)
    {
        double x = b.getX() - a.getX();
        double y = b.getY() - a.getY();
        return x*x + y*y;
    }


    public static double distance(Location a, Location b)
    {
        double x = b.getX() - a.getX();
        double y = b.getY() - a.getY();
        return Math.sqrt(x*x + y*y);
    }
}
