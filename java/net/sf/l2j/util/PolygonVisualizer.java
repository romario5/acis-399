package net.sf.l2j.util;

import net.sf.l2j.gameserver.model.location.Location;
import net.sf.l2j.gameserver.model.location.Polygon;
import net.sf.l2j.gameserver.model.location.Triangle;

import javax.vecmath.Point3f;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PolygonVisualizer extends Frame implements ActionListener
{
    private final List<Point> _points = new ArrayList<>();
    private final TextField _tf;
    private final Canvas _canvas;

    private String _initialData = "-19723;187827;-5600;-19903;187086;-5600;-19663;186807;-5600;-19018;186945;-5632;-19082;187567;-5592|-19145;187323;-5592;-19104;187420;-5592;-19161;187530;-5584;-19264;187472;-5600;-19255;187361;-5600";

    private Polygon _polygon;

    PolygonVisualizer()
    {
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        setResizable(false);
        setLayout(null);
        setVisible(true);
        setSize(600,650 + getInsets().top);

        _canvas = new Canvas();
        _canvas.setSize(600, 600);
        _canvas.setBounds(0, getInsets().top, 600, 600);
        _canvas.setBackground(Color.darkGray);
        _canvas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (_polygon == null) return;

                Graphics g = _canvas.getGraphics();
                g.setColor(Color.darkGray);
                g.fillRect(0, 0, 600, 600);
                renderPolygon(_polygon, true);

                int x = e.getX() - 15;
                int y = e.getY() - 15;

                int[] xPoints = new int[3];
                int[] yPoints = new int[3];
                Color fillColor = new Color(165, 158, 42);
                g.setColor(fillColor);
                for (Triangle t : _polygon.triangles) {
                    if (t.isInside(x, y)) {
                        xPoints[0] = t.a.getX() + 15; xPoints[1] = t.b.getX() + 15; xPoints[2] = t.c.getX() + 15;
                        yPoints[0] = t.a.getY() + 15; yPoints[1] = t.b.getY() + 15; yPoints[2] = t.c.getY() + 15;
                        g.fillPolygon(xPoints, yPoints, 3);
                        break;
                    }
                }
            }
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
        });
        add(_canvas);

        Button b = new Button("Visualize");
        b.setBounds(510,610 + getInsets().top,80,30);
        b.addActionListener(this);
        add(b);

        _tf = new TextField();
        _tf.setBounds(10,610 + getInsets().top,200,30);
        add(_tf);

        Graphics g = _canvas.getGraphics();
        g.setColor(Color.darkGray);
        g.fillRect(0, 0, 600, 600);

        Polygon polygon = parsePolygon(_initialData, 570);
        if (polygon == null) return;
        renderPolygon(polygon, true);
    }


    public static void main(String[] args)
    {
        PolygonVisualizer f = new PolygonVisualizer();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Graphics g = _canvas.getGraphics();
        g.setColor(Color.darkGray);
        g.fillRect(0, 0, 600, 600);

        Polygon polygon = parsePolygon(_tf.getText(), 570);
        if (polygon == null) return;
        renderPolygon(polygon, true);
    }





    public Polygon parsePolygon(String data, int fieldSize)
    {
        // At least one points set must be presented.
        String[] s = data.split("\\|");
        if (s.length == 0) return null;

        // Minimal polygon vertices count is 3.
        Point3f[] points = parsePoints(s[0]);
        if (points.length < 3) return null;

        // List with all points: polygon and holes. Used for scaling.
        List<Point3f> polygonPoints = new ArrayList<>(Arrays.asList(points));
        List<Point3f> allPoints = new ArrayList<>(Arrays.asList(points));


        // Prepare holes arrays.
        List<List<Location>> holes = new ArrayList<>();
        for (int i = 1; i < s.length; i++) {
            Point3f[] hole = parsePoints(s[i]);
            allPoints.addAll(Arrays.asList(hole));
            List<Location> holeLoc = new ArrayList<>();
            for (Point3f p : hole) {
                holeLoc.add(new Location((int) p.x, (int) p.y, 0));
            }
            if (hole.length >= 3) {
                holes.add(holeLoc);
            }
        }

        // Calculate scaling factor.
        int maxX = -1000000000;
        int minX = 1000000000;
        int maxY = -1000000000;
        int minY = 1000000000;
        for (Point3f p : allPoints) {
            if (p.x > maxX) maxX = (int) p.x;
            if (p.x < minX) minX = (int) p.x;
            if (p.y > maxY) maxY = (int) p.y;
            if (p.y < minY) minY = (int) p.y;
        }
        double k = Math.min(fieldSize/(double)(maxX - minX), fieldSize/(double)(maxY - minY));

        List<Location> locations = new ArrayList<>();
        for (Point3f p : polygonPoints) {
            p.x = (float) ((p.x - minX) * k);
            p.y = (float) ((p.y - minY) * k);
            locations.add(new Location((int) p.x, (int) p.y, 0));
        }

        Polygon polygon = new Polygon(locations);
        for (List<Location> hole : holes) {
            for (Location p : hole) {
                p.setX((int) ((p.getX() - minX) * k));
                p.setY((int) ((p.getY() - minY) * k));
            }
            polygon.addHole(hole);
        }

        _polygon = polygon;

        return polygon;
    }


    private Point3f[] parsePoints(String data)
    {
        String[] arr = data.split(";");
        Point3f p = new Point3f();
        List<Point3f> points = new ArrayList<>();
        int i = 0;
        for (String a : arr) {
            if (i == 0) {
                p.x = Integer.parseInt(a);
            } else if (i == 1) {
                p.y = Integer.parseInt(a);
            } else if (i == 2) {
                p.z = 0;
                i = 0;
                points.add(p);
                p = new Point3f();
                continue;
            }
            i++;
        }
        return points.toArray(new Point3f[0]);
    }




    private void renderPolygon(Polygon polygon, boolean fill)
    {
        Graphics g = _canvas.getGraphics();

        int[] xPoints = new int[3];
        int[] yPoints = new int[3];

        Color fillColor = new Color(100, 100, 0);

        if (fill) {
            g.setColor(fillColor);
            for (Triangle t : polygon.triangles) {
                xPoints[0] = t.a.getX() + 15; xPoints[1] = t.b.getX() + 15; xPoints[2] = t.c.getX() + 15;
                yPoints[0] = t.a.getY() + 15; yPoints[1] = t.b.getY() + 15; yPoints[2] = t.c.getY() + 15;
                g.fillPolygon(xPoints, yPoints, 3);
            }
        }


        for (Triangle t : polygon.triangles) {

            g.setColor(Color.red);
            g.fillOval(t.a.getX() - 3 + 15, t.a.getY() - 3 + 15, 6, 6);
            g.fillOval(t.b.getX() - 3 + 15, t.b.getY() - 3 + 15, 6, 6);
            g.fillOval(t.c.getX() - 3 + 15, t.c.getY() - 3 + 15, 6, 6);

            g.setColor(Color.yellow);
            g.drawLine(t.a.getX() + 15, t.a.getY() + 15, t.b.getX() + 15, t.b.getY() + 15);
            g.drawLine(t.a.getX() + 15, t.a.getY() + 15, t.c.getX() + 15, t.c.getY() + 15);
            g.drawLine(t.b.getX() + 15, t.b.getY() + 15, t.c.getX() + 15, t.c.getY() + 15);
        }
    }
}
