package net.sf.l2j.gameserver.model.location;

public class Vector2 {

    public double x;
    public double y;


    public Vector2(double ox, double oy)
    {
        x = ox;
        y = oy;
    }

    public double cross(Vector2 v) {
        return x * v.y - y * v.x;
    }

    public double length()
    {
        return Math.sqrt(x*x + y*y);
    }

    public double squareLength()
    {
        return x*x + y*y;
    }

    public void normalize()
    {
        double len = length();
        x =  x / (float) len;
        y =  y / (float) len;
    }

    public void normalize(double len)
    {
        x =  x / (float) len;
        y =  y / (float) len;
    }

    public void multiply(float k)
    {
        x = x * k;
        y = y * k;
    }
}