package net.sf.l2j.gameserver.model.location;

import net.sf.l2j.commons.random.Rnd;

public class DirectionRandomizer
{
    private final Vector2[] _vectors = new Vector2[3600];


    protected DirectionRandomizer()
    {
        for (int i = 0; i < 3600; i++) {
            double angle = (double) i / 10;
            _vectors[i] = new Vector2(Math.cos(Math.toRadians(angle)), Math.sin(Math.toRadians(angle)));
        }
    }


    public Vector2 getRandomDirection()
    {
        return _vectors[Rnd.get(3600)];
    }


    public static DirectionRandomizer getInstance()
    {
        return DirectionRandomizer.SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder
    {
        protected static final DirectionRandomizer INSTANCE = new DirectionRandomizer();
    }
}
