package net.sf.l2j.gameserver.model.location;

import net.sf.l2j.util.VectorMath;

public class Line
{
    public Location a;
    public Location b;

    public Line(Location p1, Location p2)
    {
        a = p1;
        b = p2;
    }

    public double length()
    {
        double x = b.getX() - a.getX();
        double y = b.getY() - a.getY();
        return Math.sqrt(x*x + y*y);
    }


    public void scaleToSize(float k)
    {
        Vector2 v = new Vector2(b.getX() - a.getX(), b.getY() - a.getY());
        v.normalize();
        v.multiply(k/2);
        a.set((int) (a.getX() - v.x), (int) (a.getY() - v.y));
        b.set((int) (b.getX() + v.x), (int) (b.getY() + v.y));
    }


    public boolean doIntersect(Line line)
    {
        return VectorMath.doIntersect(a, b, line.a, line.b);
    }
}
