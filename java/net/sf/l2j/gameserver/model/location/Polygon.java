package net.sf.l2j.gameserver.model.location;

import com.sun.j3d.utils.geometry.GeometryInfo;
import net.sf.l2j.commons.random.Rnd;
import net.sf.l2j.gameserver.geoengine.GeoEngine;

import javax.vecmath.Point3f;
import java.util.ArrayList;
import java.util.List;

public class Polygon
{
    public final List<Location> points = new ArrayList<>();
    public final List<List<Location>> holes = new ArrayList<>();
    public final List<Triangle> triangles = new ArrayList<>();

    private final List<Triangle> _randomTriangles = new ArrayList<>();
    private final List<Double> _squares = new ArrayList<>();
    private double _totalSquare = 0;

    private final int RND_ARRAY_SIZE = 500;

    public Polygon(List<Location> locations)
    {
        setLocations(locations);
    }

    public Polygon() {}

    public void setTriangles(List<Triangle> trianglesList)
    {
        triangles.clear();
        triangles.addAll(trianglesList);
    }

    public void setLocations(List<Location> locations)
    {
        points.clear();
        points.addAll(locations);
        update();
    }

    public double getSquare()
    {
        return _totalSquare;
    }

    public void addHole(List<Location> points)
    {
        addHole(points, true);
    }


    public void addHole(List<Location> points, boolean forceUpdate)
    {
        List<Location> p = new ArrayList<>(points);
        holes.add(p);
        if (forceUpdate) update();
    }


    public void update()
    {
        triangulate();
        precalculateRandomDistribution();
    }

    public void triangulate()
    {
        triangles.clear();

        int[] stripCounts = new int[holes.size() + 1];
        int[] contourCounts = new int[]{holes.size() + 1};

        stripCounts[0] = points.size();

        List<Point3f> allPoints = convertPoints(points);
        List<Location> allLocations = new ArrayList<>(points);

        int i = 1;
        for (List<Location> hole : holes) {
            allLocations.addAll(hole);
            List<Point3f> h = convertPoints(hole);
            allPoints.addAll(h);
            stripCounts[i] = hole.size();
            i++;
        }

        // Do triangulation.
        GeometryInfo gi = new GeometryInfo(GeometryInfo.POLYGON_ARRAY);
        gi.setCoordinates(allPoints.toArray(new Point3f[0]));
        gi.setStripCounts(stripCounts);
        gi.setContourCounts(contourCounts);
        gi.convertToIndexedTriangles();
        int[] indices = gi.getCoordinateIndices();

        // Create triangles
        i = 0;
        Triangle triangle = new Triangle();
        Location loc;
        for (Integer index : indices) {
            loc = allLocations.get(index);
            if (i == 0) {
                triangle.a.set(loc);
                triangle.aIndex = index;
            } else if (i == 1) {
                triangle.b.set(loc);
                triangle.bIndex = index;
            } else if (i == 2) {
                triangle.c.set(loc);
                triangle.cIndex = index;
                triangles.add(triangle);
                triangle = new Triangle();
                i = 0;
                continue;
            }
            i++;
        }
    }


    private List<Point3f> convertPoints(List<Location> locations)
    {
        List<Point3f> newPoints = new ArrayList<>();
        for (Location loc : locations) {
            Point3f p = new Point3f();
            p.x = loc.getX();
            p.y = loc.getY();
            p.z = 0;
            newPoints.add(p);
        }
        return newPoints;
    }


    public void precalculateRandomDistribution()
    {
        _randomTriangles.clear();
        _squares.clear();

        double totalSquare = 0;
        double square, k;

        int i = 0;
        for (Triangle triangle : triangles) {
            square = triangle.square();
            _squares.add(i, square);
            totalSquare += square;
            i++;
        }

        _totalSquare = totalSquare;

        double s = 0, m;
        int j = 0;
        for (i = 0; i < RND_ARRAY_SIZE; i++) {
            if (j < triangles.size()) {
                k = (_squares.get(j) / totalSquare) * RND_ARRAY_SIZE;
                m = k + s;
                if (j == _squares.size() - 1) m = RND_ARRAY_SIZE;

                if (i > m) {
                    j++;
                    s += k;
                }
            }

            _randomTriangles.add(i, triangles.get(j));
        }
    }

    public Location getRandomLocation() {
        return getRandomLocation(null);
    }

    public Location getRandomLocation(Location lastDeathLocation) {
        Location loc = _randomTriangles.get(Rnd.get(0, RND_ARRAY_SIZE - 1)).getRandomLocation();
        loc.setZ(GeoEngine.getInstance().getHeight(loc));
        return loc;
    }
}
