package net.sf.l2j.gameserver.model.location;

import net.sf.l2j.commons.random.Rnd;

public class Triangle
{
    public final Location a = new Location(0, 0, 0);
    public final Location b = new Location(0, 1, 0);
    public final Location c = new Location(1, 0, 0);

    public int aIndex = 0;
    public int bIndex = 1;
    public int cIndex = 2;

    public int[] borderIndices = new int[] {-1, -1};

    public Triangle()
    {

    }

    public Triangle(Location loc1, Location loc2, Location loc3)
    {
        a.set(loc1);
        b.set(loc2);
        c.set(loc3);
    }

    public void set(Triangle t)
    {
        aIndex = t.aIndex;
        bIndex = t.bIndex;
        cIndex = t.cIndex;
        a.set(t.a);
        b.set(t.b);
        c.set(t.c);
    }

    public void adjustZ(int minZ, int maxZ)
    {
        if (a.getZ() < minZ || a.getZ() > maxZ) a.setZ(minZ);
        if (b.getZ() < minZ || b.getZ() > maxZ) b.setZ(minZ);
        if (c.getZ() < minZ || c.getZ() > maxZ) c.setZ(minZ);
    }

    public Location getFirstBorderLocation()
    {
        if (borderIndices[0] < 1) return a;
        if (borderIndices[0] == 1) return b;
        return c;
    }

    public Location getSecondBorderLocation()
    {
        if (borderIndices[1] < 0) {
            if (borderIndices[0] < 0) return b;
            borderIndices[1] = borderIndices[0] + 1;
            if (borderIndices[1] > 2) borderIndices[1] = 0;
        }
        if (borderIndices[1] == 0) return a;
        if (borderIndices[1] == 1) return b;
        return c;
    }

    private float sign (Location p1, Location p2, Location p3)
    {
        return (p1.getX() - p3.getX()) * (p2.getY() - p3.getY())
                - (p2.getX() - p3.getX()) * (p1.getY() - p3.getY());
    }


    boolean contains (Location loc)
    {
        float d1, d2, d3;
        boolean hasNegative, hasPositive;

        d1 = sign(loc, a, b);
        d2 = sign(loc, b, c);
        d3 = sign(loc, c, a);

        hasNegative = (d1 < 0) || (d2 < 0) || (d3 < 0);
        hasPositive = (d1 > 0) || (d2 > 0) || (d3 > 0);

        return !(hasNegative && hasPositive);
    }

    public double square()
    {
        return Math.abs((a.getX() - c.getX()) * (b.getY() - a.getY()) - (a.getX() - b.getX()) * (c.getY() - a.getY())) / 2;
    }


    public Location getCenter()
    {
        return new Location(
            (a.getX() + b.getX() + c.getX()) / 3,
            (a.getY() + b.getY() + c.getY()) / 3,
                a.getZ()
        );
    }


    public Location getRandomLocation()
    {
        Vector3 _a = new Vector3();
        Vector3 _b = new Vector3();
        _a.set(
                b.getX() - a.getX(),
                b.getY() - a.getY(),
                b.getZ() - a.getZ()
        );
        _b.set(
                c.getX() - a.getX(),
                c.getY() - a.getY(),
                c.getZ() - a.getZ()
        );
        double u1 = Rnd.nextDouble();
        double u2 = Rnd.nextDouble();
        if (u1 + u2 > 1) {
            u1 = 1 - u1;
            u2 = 1 - u2;
        }
        _a.multiply((float) u1);
        _b.multiply((float) u2);
        _a.x += _b.x + a.getX();
        _a.y += _b.y + a.getY();
        _a.z += _b.z + a.getZ();
        return _a.toLocation();
    }



    static double area(int x1, int y1, int x2, int y2, int x3, int y3)
    {
        return Math.abs((x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))/2.0);
    }



    public boolean isInside(int x, int y)
    {
        /* Calculate area of triangle ABC */
        double A = area (a.getX(), a.getY(), b.getX(), b.getY(), c.getX(), c.getY());

        /* Calculate area of triangle PBC */
        double A1 = area (x, y, b.getX(), b.getY(), c.getX(), c.getY());

        /* Calculate area of triangle PAC */
        double A2 = area (a.getX(), a.getY(), x, y, c.getX(), c.getY());

        /* Calculate area of triangle PAB */
        double A3 = area (a.getX(), a.getY(), b.getX(), b.getY(), x, y);

        return (A == A1 + A2 + A3);
    }


    public boolean isInside(Location p)
    {
        return isInside(p.getX(), p.getY());
    }


    @Override
    public String toString() {
        return "(" + a + "; " + b + "; " + c + ')';
    }
}
