package net.sf.l2j.gameserver.model.spawn;

import net.sf.l2j.gameserver.geoengine.GeoEngine;
import net.sf.l2j.gameserver.network.serverpackets.ExServerPrimitive;

import java.awt.*;

public class SpawnData
{
    private final SpawnArea _area;
    private final byte[][] _data;

    public static final byte UNDEFINED   = 0x00;
    public static final byte AVAILABLE   = 0x01;
    public static final byte UNAVAILABLE = 0x02;

    private int _totalPointsStored = 0;

    public SpawnData(SpawnArea area)
    {
        _area = area;
        _data = new byte[area.boundaryWidth()][area.boundaryHeight()];
    }

    public boolean hasDataFor(int geoX, int geoY)
    {
        geoX = geoX - _area.minGeoX();
        geoY = geoY - _area.minGeoY();
        if (geoX < 0 || _data.length <= geoX) return false;
        if (geoY < 0 || _data[geoX].length <= geoY) return false;
        return _data[geoX][geoY] != UNDEFINED;
    }

    public boolean isAvailable(int geoX, int geoY)
    {
        geoX = geoX - _area.minGeoX();
        geoY = geoY - _area.minGeoY();
        if (geoX < 0 || _data.length <= geoX) return false;
        if (geoY < 0 || _data[geoX].length <= geoY) return false;
        return _data[geoX][geoY] == AVAILABLE;
    }

    public void setUnavailable(int geoX, int geoY)
    {
        geoX = geoX - _area.minGeoX();
        geoY = geoY - _area.minGeoY();
        if (geoX < 0 || _data.length <= geoX) return;
        if (geoY < 0 || _data[geoX].length <= geoY) return;
        if (_data[geoX][geoY] == UNDEFINED) _totalPointsStored++;
        _data[geoX][geoY] = UNAVAILABLE;
    }

    public void setAvailable(int geoX, int geoY)
    {
        geoX = geoX - _area.minGeoX();
        geoY = geoY - _area.minGeoY();
        if (geoX < 0 || _data.length <= geoX) return;
        if (geoY < 0 || _data[geoX].length <= geoY) return;
        if (_data[geoX][geoY] == UNDEFINED) _totalPointsStored++;
        _data[geoX][geoY] = AVAILABLE;
    }

    public int getTotalPointsStored()
    {
        return _totalPointsStored;
    }


    public void visualize(ExServerPrimitive debug)
    {
        int x, y, z;
        for (int i = 0; i < _data.length; i++) {
            for (int j = 0; j < _data[i].length; j++) {
                x = GeoEngine.getWorldX(i + _area.minGeoX());
                y = GeoEngine.getWorldY(j + _area.minGeoY());
                z = GeoEngine.getInstance().getHeightNearest(i + _area.minGeoX(), j + _area.minGeoY(), _area.minZ());
                debug.addPoint("", _data[i][j] == UNDEFINED ? Color.RED : Color.GREEN, false, x, y, z + 20);
            }
        }
    }
}