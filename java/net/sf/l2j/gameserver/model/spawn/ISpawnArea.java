package net.sf.l2j.gameserver.model.spawn;

import net.sf.l2j.gameserver.model.location.Location;

import java.util.List;

public interface ISpawnArea
{
    /**
     * @return List of all spawns.
     */
    List<Spawn> getSpawns();

    /**
     * Enables spawn area.
     * Does all spawns on call.
     */
    void enable();

    /**
     * Disables spawn area.
     * Removes all creatures and disables their respawn state.
     */
    void disable();

    /**
     * @return True if spawn area is enabled.
     */
    boolean isEnabled();

    /**
     * Spawns all creatures.
     * If creature already spawned and alive it will be left unchanged.
     */
    void spawnAll();

    /**
     * Adds spawn to the area.
     * @param spawn Spawn to be added.
     */
    void addSpawn(Spawn spawn);

    /**
     * @param lastDeathLocation Location of the last creature death for better randomization.
     * @return Random location inside the area.
     */
    Location getRandomLocation(Location lastDeathLocation);
}
