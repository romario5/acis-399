package net.sf.l2j.gameserver.model.spawn;

import net.sf.l2j.commons.logging.CLogger;
import net.sf.l2j.commons.random.Rnd;

import java.util.ArrayList;
import java.util.List;

public class SpawnGroup
{
    private static final CLogger LOGGER = new CLogger(SpawnGroup.class.getName());

    private final String _id;
    private final List<Spawn> _spawns = new ArrayList<>();
    private final List<SpawnArea> _spawnAreas = new ArrayList<>();
    private final List<SpawnArea> _areasRandom = new ArrayList<>();
    private int _maxNpc = 1;
    private int _spawnedCount = 0;

    public SpawnGroup(String id)
    {
        _id = id;
    }


    public String getId()
    {
        return _id;
    }

    public void setMaxNpcCount(int maxNpcCount)
    {
        _maxNpc = maxNpcCount;
    }

    public void addSpawnArea(SpawnArea area)
    {
        _spawnAreas.add(area);
    }

    public void addSpawn(Spawn spawn)
    {
        _spawns.add(spawn);
    }

    public void requestRespawn(Spawn spawn)
    {
        if (_spawns.size() == 0 || _spawnAreas.size() == 0) return;
        _spawnedCount--;
        if (_spawnedCount < 0) _spawnedCount = 0;

        if (_maxNpc > _spawnedCount && _spawns.size() - 1 > _spawnedCount) {
            for (Spawn s : _spawns) {
                if (s != spawn && s.getNpc().isDecayed()) {
                    spawn = s;
                    break;
                }
            }
        }

        setRandomSpawnArea(spawn);
        spawn.doRealRespawn();
    }


    public void doSpawn()
    {
        prepareRandomDistribution();
        if (_spawns.size() == 0 || _spawnAreas.size() == 0) {
            LOGGER.error("No spawns or areas for group " + _id);
            return;
        }
        for (int i = 0; i < _spawns.size(); i++) {
            if (i >= _maxNpc) return;
            Spawn spawn = _spawns.get(i);
            setRandomSpawnArea(spawn);
            spawn.setRespawnState(true);
            spawn.doSpawn(false);
            _spawnedCount++;
        }
    }

    private void setRandomSpawnArea(Spawn spawn)
    {
        if (_spawnAreas.size() == 1) {
            spawn.setSpawnArea(_spawnAreas.get(0));
        } else if (_spawnAreas.size() > 0) {
            spawn.setSpawnArea(_areasRandom.get(Rnd.get(0, _areasRandom.size() - 1)));
        }
    }

    private void prepareRandomDistribution()
    {
        List<Double> k = new ArrayList<>();
        double totalSquare = 0;
        double s = 0;
        for (SpawnArea a : _spawnAreas) {
            totalSquare += a.getSquare();
        }
        for (SpawnArea a : _spawnAreas) {
            s += a.getSquare() / totalSquare * 30;
            k.add(s);
            if (_id.equals("gludio05_1923_34m12")) {
                LOGGER.info(a.getId() + " = " + a.getSquare() + " (" + s + ")");
            }
        }
        int j = 0;
        for (int i = 0; i < 30; i++) {
            _areasRandom.add(_spawnAreas.get(j));
            if (i >= k.get(j) && j < _spawnAreas.size() - 1) {
                j++;
            }
            if (_id.equals("gludio05_1923_34m12")) {
                LOGGER.info(_areasRandom.get(i).getId());
            }
        }

    }
}
