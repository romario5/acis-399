package net.sf.l2j.gameserver.model.spawn;

import net.sf.l2j.commons.logging.CLogger;
import net.sf.l2j.commons.random.Rnd;
import net.sf.l2j.gameserver.data.sql.SpawnTable;
import net.sf.l2j.gameserver.geoengine.GeoEngine;
import net.sf.l2j.gameserver.model.location.Location;
import net.sf.l2j.gameserver.model.location.Polygon;
import net.sf.l2j.gameserver.model.location.Triangle;
import net.sf.l2j.gameserver.model.location.Vector3;
import net.sf.l2j.gameserver.network.serverpackets.ExServerPrimitive;
import net.sf.l2j.util.VectorMath;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SpawnArea implements ISpawnArea
{
    private static final CLogger LOGGER = new CLogger(Spawn.class.getName());

    private final int RND_ARRAY_SIZE = 100;

    private final String _id;
    private int _minZ = 0;
    private int _maxZ = 0;
    private final List<Location> _locations = new ArrayList<>();
    private final List<List<Location>> _holes = new ArrayList<>();
    private final Polygon _polygon;
    private final List<Triangle> _randomTriangles = new ArrayList<>();
    private final List<Double> _squares = new ArrayList<>();
    private final List<Spawn> _spawns = new ArrayList<>();
    private boolean _enabled = true;
    private Location _center;
    private double _square = 0;

    private final Color _borderNormalColor = new Color(50, 50, 50);
    private final Color _lineNormalColor   = new Color(64, 64, 64);
    private final Color _lineActiveColor   = new Color(0, 255, 0);
    private final Color _pointNormalColor  = new Color(60, 60, 60);

    private int _minX = Integer.MAX_VALUE;
    private int _maxX = Integer.MIN_VALUE;
    private int _minY = Integer.MAX_VALUE;
    private int _maxY = Integer.MIN_VALUE;

    private int _boundaryWidth = 0;
    private int _boundaryHeight = 0;

    private final SpawnData _spawnData;

    public SpawnArea(String id, List<Location> locations, int minZ, int maxZ)
    {
        _id = id;
        _locations.addAll(locations);
        _polygon = new Polygon(_locations);
        precalculateRandomDistribution();
        _minZ = minZ;
        _maxZ = maxZ;
        getCenter();
        detectBoundaries();
        _spawnData = new SpawnData(this);
    }

    public SpawnArea(List<Location> locations)
    {
        _id = "";
        _locations.addAll(locations);
        getCenter();
        _polygon = new Polygon(_locations);
        precalculateRandomDistribution();
        detectBoundaries();
        _spawnData = new SpawnData(this);
    }

    public SpawnArea(String id, List<Location> locations)
    {
        _id = id;
        _locations.addAll(locations);
        getCenter();
        _polygon = new Polygon(_locations);
        precalculateRandomDistribution();
        detectBoundaries();
        _spawnData = new SpawnData(this);
    }

    private void detectBoundaries()
    {
        for (Location loc : _locations) {
            if (_minX > loc.getX()) _minX = loc.getX();
            if (_maxX < loc.getX()) _maxX = loc.getX();
            if (_minY > loc.getY()) _minY = loc.getY();
            if (_maxY < loc.getY()) _maxY = loc.getY();
        }

        _minX = GeoEngine.getGeoX(_minX);
        _maxX = GeoEngine.getGeoX(_maxX);
        _minY = GeoEngine.getGeoY(_minY);
        _maxY = GeoEngine.getGeoY(_maxY);

        _boundaryWidth = _maxX - _minX + 1;
        _boundaryHeight = _maxY - _minY + 1;
    }

    public SpawnData getSpawnData()
    {
        return _spawnData;
    }

    public int minZ()
    {
        return _minZ;
    }

    public int maxZ() {
        return _maxZ;
    }

    public int boundaryWidth()
    {
        return _boundaryWidth;
    }

    public int boundaryHeight()
    {
        return _boundaryHeight;
    }

    public int minGeoX()
    {
        return _minX;
    }

    public int maxGeoX()
    {
        return _maxX;
    }

    public int minGeoY()
    {
        return _minY;
    }

    public int maxGeoY()
    {
        return _maxY;
    }

    public void copyPointsFrom(SpawnArea area)
    {
        _locations.clear();
        _holes.clear();

        for (Location loc : area.getLocations()) {
            _locations.add(loc.clone());
        }
        _polygon.setLocations(_locations);
        _polygon.holes.clear();

        for (List<Location> hole : area.getHoles()) {
            List<Location> h = new ArrayList<>();
            for (Location loc : hole) {
                h.add(loc.clone());
            }
            _holes.add(h);
            _polygon.holes.add(h);
        }
        _polygon.update();
        precalculateRandomDistribution();
        getCenter();
    }

    public void recalculate()
    {
        getCenter();
        _polygon.setLocations(_locations);
        precalculateRandomDistribution();
    }

    public String getId()
    {
        return _id;
    }

    public Location getCenter()
    {
        if (_center == null) {
            _center = new Location(0, 0, 0);
            int z;
            int lastNormalZ = _minZ;
            for (Location loc : _locations) {
                z = loc.getZ();
                if (z < _minZ || z > _maxZ) {
                    z = lastNormalZ;
                } else {
                    lastNormalZ = z;
                }
                _center.setX(_center.getX() + loc.getX());
                _center.setY(_center.getY() + loc.getY());
                _center.setZ(_center.getZ() + z);
            }
            _center.setX(Math.round((float) _center.getX() / (float) _locations.size()));
            _center.setY(Math.round((float) _center.getY() / (float) _locations.size()));
            _center.setZ(Math.round((float) _center.getZ() / (float) _locations.size()));
        }
        return _center;
    }

    public double getSquare()
    {
        return _square;
    }

    public List<Location> getLocations()
    {
        return _locations;
    }

    public List<List<Location>> getHoles()
    {
        return _holes;
    }


    public void precalculateRandomDistribution()
    {
        if (_locations.size() < 3) return;
        _randomTriangles.clear();
        _squares.clear();

        double totalSquare = 0;
        double square, k;

        int i = 0;
        for (Triangle triangle : _polygon.triangles) {
            square = triangle.square();
            _squares.add(i, square);
            totalSquare += square;
            i++;
        }

        _square = Math.round(totalSquare);

        double s = 0, m;
        int j = 0;
        double size = _polygon.triangles.size() * RND_ARRAY_SIZE;
        for (i = 0; i < size; i++) {
            if (j < _polygon.triangles.size()) {
                k = (_squares.get(j) / totalSquare) * size;
                m = k + s;
                if (j == _squares.size() - 1) m = size;

                if (i > m) {
                    j++;
                    s += k;
                }
            }

            _randomTriangles.add(i, _polygon.triangles.get(j));
        }
    }


    public void addHole(List<Location> points)
    {
        List<Location> p = new ArrayList<>(points);
        _holes.add(p);
        _polygon.addHole(points);
    }

    public Polygon getPolygon()
    {
        return _polygon;
    }

    public ExServerPrimitive showDebug(ExServerPrimitive debug, boolean isActive)
    {
        Color lineColor = _lineNormalColor;
        List<Integer> renderedLines = new ArrayList<>();
        int id;
        if (isActive) lineColor = _lineActiveColor;
        Triangle t = new Triangle();
        for (Triangle triangle : _polygon.triangles) {
            t.set(triangle);
            t.adjustZ(_minZ, _maxZ);

            id = t.aIndex * 100000 + t.bIndex;

            if (!renderedLines.contains(id)) {
                renderLine(debug, lineColor, t.a, t.b);
                renderedLines.add(id);
                renderedLines.add(t.bIndex * 100000 + t.aIndex);
            }
            id = t.bIndex * 100000 + t.cIndex;
            if (!renderedLines.contains(id)) {
                renderLine(debug, lineColor, t.b, t.c);
                renderedLines.add(id);
                renderedLines.add(t.cIndex * 100000 + t.bIndex);
            }
            id = t.cIndex * 100000 + t.aIndex;
            if (!renderedLines.contains(id)) {
                renderLine(debug, lineColor, t.c, t.a);
                renderedLines.add(id);
                renderedLines.add(t.aIndex * 100000 + t.cIndex);
            }

            debug.addPoint("(" + triangle.square() + ")", Color.YELLOW, true, triangle.getCenter());
        }
        Location loc2 = new Location(0, 0, 0);
        Location loc3 = new Location(0, 0, 0);
        loc3.set(_locations.get(_locations.size() - 1));
        for (Location loc : _locations) {
            loc2.set(loc);
            loc2.setZ(_minZ);
            loc3.setZ(_minZ);
            debug.addLine(Color.BLUE, loc2, loc3);
            loc2.setZ(_maxZ);
            loc3.setZ(_maxZ);
            debug.addLine(Color.BLUE, loc2, loc3);
            loc3.set(loc2);
            loc2.setZ(_minZ);
            loc3.setZ(_maxZ);
            debug.addLine(Color.BLUE, loc2, loc3);

            loc2.set(loc);
            loc2.setZ(loc.getZ() - 20);
            if (loc2.getZ() < _minZ || loc2.getZ() > _maxZ) loc2.setZ(_minZ);
            debug.addPoint(lineColor, loc2);
        }
        debug.addPoint(getId() + " (" + getSquare() + ")", Color.YELLOW, true, getCenter());
        return debug;
    }

    public void renderPoint(ExServerPrimitive debug, Color color, Location loc)
    {
        Location loc2 = new Location(loc);
        loc2.setZ(loc.getZ() - 20);
        debug.addPoint(color, loc2);
    }

    public void renderStraightLine(ExServerPrimitive debug, Color color, Location p1, Location p2)
    {
        Location loc1 = new Location(p1);
        Location loc2 = new Location(p2);
        loc1.setZ(loc1.getZ() - 20);
        loc2.setZ(loc2.getZ() - 20);
        debug.addLine(color, loc1, loc2);
    }

    private void renderLine(ExServerPrimitive debug, Color color, Location p1, Location p2)
    {
        Vector3 v = new Vector3(p1, p2);
        double len = v.length();
        int steps = 5;
        int maxStepLen = 100;
        double stepLen = len / steps;
        if (stepLen > maxStepLen) {
            steps = (int) Math.round(len / maxStepLen);
            stepLen = len / steps;
        }

        v.normalize();
        Vector3 v2 = new Vector3();
        v2.set(v);
        v2.multiply((float) stepLen);
        Location start = p1.clone();
        start.setZ(start.getZ() - 20);
        int lastZ = _minZ;
        if (start.getZ() < _minZ || start.getZ() > _maxZ) start.setZ(lastZ);
        Location end = new Location(0, 0, 0);
        for (int i = 1; i <= steps; i++) {
            if (i == steps) {
                end.set(p2);
                end.setZ(end.getZ() - 20);
                debug.addLine(color, start, end);
                break;
            }
            end.set(start.getX() + (int) v2.x, start.getY() + (int) v2.y, p1.getZ() + (int) v2.z);
            end.setZ(GeoEngine.getInstance().getHeight(end) - 20);
            if (end.getZ() < _minZ || end.getZ() > _maxZ) end.setZ(lastZ);
            lastZ = end.getZ();
            debug.addLine(color, start, end);
            start.set(end);
        }
    }


    @Override
    public List<Spawn> getSpawns() {
        return _spawns;
    }

    @Override
    public void enable() {
        if (_enabled) return;
        _enabled = true;
        spawnAll();
    }

    @Override
    public void disable() {
        if (!_enabled) return;
        _enabled = false;
        for (Spawn spawn : _spawns) {
            spawn.setRespawnState(false);
            spawn.getNpc().deleteMe();
        }
    }

    public void updateSpawns()
    {
        for (Spawn spawn : _spawns) {
            spawn.setRespawnState(false);
            spawn.getNpc().deleteMe();
        }
        spawnAll();
    }

    @Override
    public boolean isEnabled() {
        return _enabled;
    }

    @Override
    public void spawnAll() {
        for (Spawn spawn : _spawns) {
            spawn.setRespawnState(true);
            if (spawn.getNpc().isDecayed() || spawn.getNpc().isDead()) {
                spawn.doSpawn(false);
            }
        }
    }


    public Spawn addSpawn(int npcId, int respawnDelay, int respawnRandomizeTime, int walkingRange)
    {
        Spawn spawn;
        try {
            spawn = new Spawn(npcId);
            spawn.setRespawnDelay(respawnDelay);
            spawn.setRespawnRandom(respawnRandomizeTime);
            spawn.setRespawnState(true);
            addSpawn(spawn);
        } catch (Exception e) {
            LOGGER.error("Error on adding spawn to the spawn area: " + e.getMessage());
            return null;
        }
        return spawn;
    }


    @Override
    public void addSpawn(Spawn spawn) {
        addSpawn(spawn, true);
    }


    public void addSpawn(Spawn spawn, boolean spawnIfEnabled) {
        spawn.setSpawnArea(this);
        _spawns.add(spawn);
        SpawnTable.getInstance().addSpawn(spawn, false);
        if (spawnIfEnabled && _enabled) {
            spawn.doSpawn(false);
        }
    }

    public Location getRandomLocation(Location lastDeathLocation) {
        Location loc;
        int tries = 0;
        do {
            loc = _randomTriangles.get(Rnd.get(0, _randomTriangles.size() - 1)).getRandomLocation();
            loc.setZ(_minZ);
            if (GeoEngine.getInstance().checkSpawnLocation(loc, this, null)) {
                loc.setZ(GeoEngine.getInstance().getHeight(loc));
            } else {
                loc = null;
            }
            tries++;
        } while (tries < 20 && loc == null);
        return loc;
    }

    public Location getRawRandomLocation()
    {
        Location loc = _randomTriangles.get(Rnd.get(0, _randomTriangles.size() - 1)).getRandomLocation();
        loc.setZ(_minZ);
        loc.setZ(GeoEngine.getInstance().getHeight(loc));
        return loc;
    }

    public boolean addPoint(Location c)
    {
        double minDist = 1000000;
        double dist;
        Location lastLoc = null;
        Location a = null;
        Location d;

        List<Location> locations = new ArrayList<>(_locations);
        locations.add(_locations.get(0));

        for (Location b : locations) {
            if (a == null) {
                a = b;
                continue;
            }
            d = VectorMath.getProjection(a, b, c);
            if (d != null) {
                dist = c.distance2D(d);

                if (dist < minDist) {
                    minDist = dist;
                    lastLoc = a;
                }
            }
            a = b;
        }

        if (lastLoc != null) {
            List<Location> newPoints = new ArrayList<>();
            for (Location loc : _locations) {
                newPoints.add(loc);
                if (loc == lastLoc) {
                    c.setZ(GeoEngine.getInstance().getHeight(c));
                    newPoints.add(c);
                }
            }
            _locations.clear();
            _locations.addAll(newPoints);
            _polygon.setLocations(_locations);
            _polygon.update();
            precalculateRandomDistribution();
            getCenter();

            return true;
        }

        return false;
    }





    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Location loc : _locations) {
            if (i > 0) sb.append(";");
            sb.append(loc.getX()).append(";").append(loc.getY()).append(";").append(loc.getZ());
            i++;
        }

        for (List<Location> hole : _holes) {
            if (hole.size() < 3) continue;
            sb.append("|");
            i = 0;
            for (Location loc : hole) {
                if (i > 0) sb.append(";");
                sb.append(loc.getX()).append(";").append(loc.getY()).append(";").append(loc.getZ());
                i++;
            }
        }

        return sb.toString();
    }
}
