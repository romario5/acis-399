package net.sf.l2j.gameserver.handler.admincommandhandlers;

import net.sf.l2j.commons.data.Pagination;
import net.sf.l2j.commons.lang.StringUtil;
import net.sf.l2j.gameserver.data.sql.SpawnAreasTable;
import net.sf.l2j.gameserver.geoengine.GeoEngine;
import net.sf.l2j.gameserver.handler.IAdminCommandHandler;
import net.sf.l2j.gameserver.model.actor.Player;
import net.sf.l2j.gameserver.model.location.Location;
import net.sf.l2j.gameserver.model.spawn.SpawnArea;
import net.sf.l2j.gameserver.network.serverpackets.ExServerPrimitive;
import net.sf.l2j.gameserver.network.serverpackets.NpcHtmlMessage;

import java.awt.*;
import java.util.List;
import java.util.*;

public class AdminAreas implements IAdminCommandHandler
{
    private static final String[] ADMIN_COMMANDS =
    {
        "admin_areas",
        "admin_areas_show",
        "admin_area",
        "admin_area_create",
        "admin_areas_check"
    };

    private static final Map<Player, AreasVisualizer> _shownFor = new HashMap<>();

    @Override
    public void useAdminCommand(String command, Player player)
    {
        ExServerPrimitive debug = player.getDebugPacket("AREA_GEO");
        debug.reset();
        debug.sendTo(player);

        if (command.equals("admin_areas"))
        {
            showMainPage(player, getPage(command));
        }
        else if (command.equals("admin_areas_check")) {
            AreasVisualizer visualizer = getVisualizer(player);
            visualizer.checkLocation(player.getPosition());
            showMainPage(player, 1);
        }
        else if (command.equals("admin_areas_show"))
        {
            AreasVisualizer visualizer = getVisualizer(player);

            if (visualizer.isEnabled()) {
                visualizer.disable();
                player.removeRegionUpdateHandler(visualizer);
            } else {
                visualizer.enable();
                player.onRegionUpdate(visualizer);
                visualizer.run();
            }
            showMainPage(player, 1);
        }
        else if (command.startsWith("admin_area")) {
            final StringTokenizer st = new StringTokenizer(command, " ");
            st.nextToken();

            if (st.hasMoreTokens()) {
                String action = st.nextToken();

                if (action.equals("select"))
                {
                    if (st.hasMoreTokens()) {
                        SpawnArea area = SpawnAreasTable.getInstance().getArea(st.nextToken());
                        if (area != null) {
                            AreasVisualizer visualizer = getVisualizer(player);
                            visualizer.setActiveArea(area);
                        }
                    }
                }
                else if (action.equals("deselect"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    visualizer.setActiveArea(null);
                }
                else if (action.equals("geo"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    SpawnArea area = visualizer.getActiveArea();
                    if (area != null) {
                        debug = player.getDebugPacket("AREA_GEO");
                        Location start = null;
                        Location first = null;
                        for (Location loc : area.getLocations()) {
                            if (first == null) first = loc;
                            if (start != null) {
                                GeoEngine.getInstance().traceLine(start, loc, debug);
                            }
                            start = loc;
                        }
                        if (start != null)
                            GeoEngine.getInstance().traceLine(start, first, debug);

                        debug.sendTo(player);
                    }

                    showMainPage(player, 1);
                }
                else if (action.equals("addHole"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    visualizer.startMakingHole();
                }
                else if (action.equals("selectPoint"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    visualizer.startPointSelecting();
                    showPointsSelector(visualizer.getActiveArea(), player);
                    return;
                }
                else if (action.equals("movePoint"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    visualizer.startPointMoving();
                }
                else if (action.equals("addPoint"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    visualizer.startPointAdding();
                }
                else if (action.equals("copyTo"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    visualizer.startCopyTo();
                }
                else if (action.equals("showRandom"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    visualizer.showRandomDistribution();
                }
                else if (action.equals("deleteHoles"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    SpawnArea area = visualizer.getActiveArea();
                    if (area != null) {
                        SpawnArea a = new SpawnArea(area.getLocations());
                        area.copyPointsFrom(a);
                        SpawnAreasTable.getInstance().saveSpawnArea(area);
                        visualizer.run();
                    }
                }
                else if (action.equals("info"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    SpawnArea area = visualizer.getActiveArea();
                    if (area != null) {
                        showAreaInfo(area, player);
                        return;
                    }
                }
                else if (action.equals("points"))
                {
                    AreasVisualizer visualizer = getVisualizer(player);
                    SpawnArea area = visualizer.getActiveArea();
                    if (area != null) {
                        showPointsSelector(area, player);
                        return;
                    }
                }
                else if (action.equals("point"))
                {
                    if (st.hasMoreTokens()) {
                        int index = Integer.parseInt(st.nextToken());
                        AreasVisualizer visualizer = getVisualizer(player);
                        visualizer.selectAreaPoint(index);
                    }
                }


            }

            showMainPage(player, 1);
        }
    }



    private void showAreaInfo(SpawnArea area, Player player)
    {
        // Load static htm.
        final NpcHtmlMessage html = new NpcHtmlMessage(0);
        html.setFile("data/html/admin/areas/area_info.htm");

        final StringBuilder sb = new StringBuilder(5000);
        sb.append("<table width=270><tr><td width=150></td><td width=120></td></tr>");


        StringUtil.append(sb, "<tr><td>ID</td><td><font color=\"LEVEL\">", area.getId(), "</font></td></tr>");
        StringUtil.append(sb, "<tr><td>Points</td><td><font color=\"LEVEL\">", area.getLocations().size(), "</font></td></tr>");
        StringUtil.append(sb, "<tr><td>Holes</td><td><font color=\"LEVEL\">", area.getHoles().size(), "</font></td></tr>");
        StringUtil.append(sb, "<tr><td>Triangles</td><td><font color=\"LEVEL\">", area.getPolygon().triangles.size(), "</font></td></tr>");
        StringUtil.append(sb, "<tr><td>Spawns</td><td><font color=\"LEVEL\">", area.getSpawns().size(), "</font></td></tr>");

        sb.append("</table>");

        html.replace("%areaInfo%", sb.toString());
        player.sendPacket(html);
    }



    public void showPointsSelector(SpawnArea area, Player player)
    {
        // Load static htm.
        final NpcHtmlMessage html = new NpcHtmlMessage(0);
        html.setFile("data/html/admin/areas/area_points.htm");

        final StringBuilder sb = new StringBuilder(5000);
        sb.append("<table width=270><tr><td width=150></td><td width=120></td></tr>");

        int i = 0;
        for (Location loc : area.getLocations()) {
            StringUtil.append(sb, "<tr><td><a action=\"bypass -h admin_area point ", i, "\">", i, "</td><td><font color=\"LEVEL\">", loc.toString(), "</font></td></tr>");
            i++;
        }

        sb.append("</table>");

        html.replace("%areaPoints%", sb.toString());
        player.sendPacket(html);
    }


    private int getPage(String command)
    {
        final StringTokenizer st = new StringTokenizer(command, " ");
        st.nextToken();
        int page = 1;
        if (st.hasMoreTokens()) {
            final String param = st.nextToken();
            if (StringUtil.isDigit(param)) {
                page = Integer.parseInt(param);
            }
        }
        return page;
    }


    @Override
    public String[] getAdminCommandList() {
        return ADMIN_COMMANDS;
    }




    public void showMainPage(Player player, int page)
    {
        AreasVisualizer visualizer = getVisualizer(player);
        SpawnArea activeArea = visualizer.getActiveArea();

        // Fetch spawn regions around the player.
        List<SpawnArea> areas = SpawnAreasTable.getInstance().getAreasNearPlayer(player);

        // Load static htm.
        final NpcHtmlMessage html = new NpcHtmlMessage(0);
        html.setFile("data/html/admin/areas/menu.htm");

        final StringBuilder sb = new StringBuilder(5000);
        sb.append("<table width=270><tr><td width=225></td><td width=45></td></tr>");

        if (areas.size() == 0) {
            player.sendMessage("No areas found");
            sb.append("<tr><td>No spawn areas near.</td></tr></table>");
        } else {
            final Pagination<SpawnArea> list = new Pagination<>(areas.stream(), page, PAGE_LIMIT_10);
            for (SpawnArea area : list) {
                if (activeArea == area) {
                    StringUtil.append(sb, "<tr><td><font color=\"LEVEL\"><a action=\"bypass -h admin_area select ", area.getId(), "\">", area.getId(), "</a></font></td><td></td></tr>");
                } else {
                    StringUtil.append(sb, "<tr><td><a action=\"bypass -h admin_area select ", area.getId(), "\">", area.getId(), "</a></td><td></td></tr>");
                }
            }
            sb.append("</table>");

            list.generateSpace(sb);
            list.generatePages(sb, "bypass admin_areas %page%");
        }

        html.replace("%areasList%", sb.toString());
        player.sendPacket(html);
    }




    public AreasVisualizer getVisualizer(Player player)
    {
        return getVisualizer(player, true);
    }


    public AreasVisualizer getVisualizer(Player player, boolean createIfAbsent)
    {
        AreasVisualizer visualizer = _shownFor.get(player);
        if (visualizer == null && createIfAbsent) {
            visualizer = new AreasVisualizer(player);
            _shownFor.put(player, visualizer);
        }
        return visualizer;
    }


    private void renderLines(List<Location> points, ExServerPrimitive debug)
    {
        if (points.size() == 0) return;
        Location start = null;
        Color color = Color.BLUE;
        List<Location> locations = new ArrayList<>();
        for (Location loc : points) locations.add(new Location(loc));
        for (Location loc : locations) {
            loc.setZ(loc.getZ() - 20);
            if (start != null) {
                debug.addLine(Color.RED, start, loc);
            }
            debug.addPoint(color, loc);
            color = Color.RED;
            start = loc;
        }
        debug.addLine(Color.RED, start, locations.get(0));
    }


    public class AreasVisualizer implements Runnable
    {
        public static final int MODE_IDLE = 0;
        public static final int MODE_CREATE_HOLE = 1;
        public static final int MODE_MOVE_POINT = 2;
        public static final int MODE_DELETE_POINT = 3;
        public static final int MODE_ADD_POINT = 4;
        public static final int MODE_COPY_TO = 5;
        public static final int MODE_SELECT_POINT = 6;

        private int _mode = MODE_IDLE;
        private Location _activeLocation = null;
        private boolean _enabled = false;
        private boolean _randomShown = false;
        private final Player _player;
        private final Map<SpawnArea, ExServerPrimitive> _debugs = new HashMap<>();
        private SpawnArea _activeArea = null;
        private List<Location> _holePoints = null;
        ExServerPrimitive _tmpDebug = null;


        public AreasVisualizer(Player player)
        {
            _player = player;
        }

        public boolean isEnabled()
        {
            return _enabled;
        }

        public void enable()
        {
            _enabled = true;
        }

        public void disable()
        {
            hideAll();
            hideRandomDistribution();
            _enabled = false;
        }

        public void setActiveArea(SpawnArea area)
        {
            if (_mode == MODE_COPY_TO && _activeArea != null) {
                area.copyPointsFrom(_activeArea);
                SpawnAreasTable.getInstance().saveSpawnArea(area);
                _mode = MODE_IDLE;
            } else {
                _activeArea = area;
            }
            if (area == null) {
                ExServerPrimitive d = _player.getDebugPacket("AREA_RANDOM_DISTRIBUTION");
                d.reset();
                d.sendTo(_player);
            }
            run();
        }

        public SpawnArea getActiveArea()
        {
            return _activeArea;
        }

        public void selectAreaPoint(int index)
        {
            if (_activeArea == null) return;
            int i = 0;
            for (Location loc : _activeArea.getLocations()) {
                if (i == index) {
                    _activeLocation = loc;
                    run();
                    return;
                }
                i++;
            }
        }

        public void hideAll()
        {
            ExServerPrimitive debug = _player.getDebugPacket("AREA_CHECK");
            debug.reset();
            debug.sendTo(_player);
            for (Map.Entry<SpawnArea, ExServerPrimitive> entry : _debugs.entrySet()) {
                if (!entry.getValue().isEmpty()) {
                    entry.getValue().reset();
                    entry.getValue().sendTo(_player);
                }
            }
            _debugs.clear();
        }


        public void startPointSelecting()
        {
            if (_activeArea != null) {
                _mode = MODE_SELECT_POINT;
                _activeLocation = null;
            }
        }

        public void startMakingHole()
        {
            if (_activeArea != null) {
                _mode = MODE_CREATE_HOLE;
                _holePoints = new ArrayList<>();
            }
        }

        public void startPointMoving()
        {
            if (_activeArea != null) {
                if (_activeLocation == null) {
                    _player.sendMessage("Select point first.");
                    return;
                }
                _mode = MODE_MOVE_POINT;
            }
        }

        public void startPointAdding()
        {
            if (_activeArea != null) {
                _activeLocation = null;
                _mode = MODE_ADD_POINT;
            }
        }

        public void startCopyTo()
        {
            if (_activeArea != null) {
                _mode = MODE_COPY_TO;
            }
        }



        public void checkLocation(Location loc)
        {
            if (_activeArea == null) {
                _player.sendMessage("Select spawn area before using this tool.");
                return;
            }
            ExServerPrimitive debug = _player.getDebugPacket("AREA_CHECK");
            debug.reset();
            _activeArea.getSpawnData().visualize(debug);
            debug.sendTo(_player);

//            Location rndLoc = _activeArea.getRawRandomLocation();
//            GeoEngine.getInstance().checkSpawnLocation(loc, _activeArea, debug);
//            debug.sendTo(_player);
        }

        public void showRandomDistribution()
        {
            if (_randomShown) {
                hideRandomDistribution();
                return;
            }

            hideRandomDistribution();
            if (_activeArea == null) return;
            hideAll();
            ExServerPrimitive debug = _player.getDebugPacket("AREA_RANDOM_DISTRIBUTION");
            debug.reset();
            long count = Math.min(Math.round(_activeArea.getSquare() / 100), 20000);
            GeoEngine.resetMeasureCounter();
            double t = System.currentTimeMillis();
            int usedPointsCount = 0;
            for (int i = 0; i < count; i++) {
                Location loc = _activeArea.getRawRandomLocation();
                if (GeoEngine.getInstance().checkSpawnLocation(loc, _activeArea, null)) {
                    debug.addPoint(i + "", Color.GREEN, false, loc);
                    usedPointsCount++;
                }
            }
            _player.sendMessage("Generated " + count + " points in " + Math.round(System.currentTimeMillis() - t) + " ms");

            int totalCache = _activeArea.boundaryWidth() * _activeArea.boundaryHeight();
            _player.sendMessage("Points got from cache: " + GeoEngine.getMeasureCounter() + " / " + count + " (" + Math.round((float) GeoEngine.getMeasureCounter() / count * 100) + "%)");
            debug.sendTo(_player);
            _randomShown = true;
        }

        public void hideRandomDistribution()
        {
            _randomShown = false;
            ExServerPrimitive d = _player.getDebugPacket("AREA_RANDOM_DISTRIBUTION");
            d.reset();
            d.sendTo(_player);
        }

        public boolean processClick(Location loc)
        {
            if (_activeArea == null) {
                _mode = MODE_IDLE;
                return false;
            }

            if (_mode == MODE_SELECT_POINT) {
                _activeLocation = null;
                for (Location p : _activeArea.getLocations()) {
                    if (p.distance2D(loc) <= 50) {
                        _activeLocation = p;
                        run();
                        _mode = MODE_IDLE;
                        return true;
                    }
                }
                if (_activeLocation == null) {
                    for (List<Location> hole : _activeArea.getHoles()) {
                        for (Location p : hole) {
                            if (p.distance2D(loc) <= 50) {
                                _activeLocation = p;
                                run();
                                _mode = MODE_IDLE;
                                return true;
                            }
                        }
                    }
                }
            }
            else if (_mode == MODE_MOVE_POINT)
            {
                if (_activeLocation != null) {
                    _activeLocation.setX(loc.getX());
                    _activeLocation.setY(loc.getY());
                    _activeLocation.setZ(GeoEngine.getInstance().getHeight(loc));
                    _activeArea.recalculate();
                    run();
                    _activeLocation = null;
                    _activeArea.updateSpawns();
                    SpawnAreasTable.getInstance().saveSpawnArea(_activeArea);
                }
                _mode = MODE_IDLE;
                return true;
            }

            if (_mode == MODE_ADD_POINT) {
                _mode = MODE_IDLE;
                if (_activeArea.addPoint(loc)) {
                    run();
                    SpawnAreasTable.getInstance().saveSpawnArea(_activeArea);
                    return true;
                }
                return false;
            }

            if (_mode == MODE_CREATE_HOLE && _holePoints != null) {
                loc.setZ(GeoEngine.getInstance().getHeight(loc));
                if (_holePoints.size() < 3) {
                    _holePoints.add(loc);
                    _tmpDebug = _player.getDebugPacket("AREA_EDIT");
                    renderLines(_holePoints, _tmpDebug);
                    _tmpDebug.sendTo(_player);
                    return true;
                }

                if (_holePoints.get(0).distance2D(loc.getX(), loc.getY()) > 50) {
                    _holePoints.add(loc);
                    if (_tmpDebug != null) {
                        _tmpDebug.reset();
                        renderLines(_holePoints, _tmpDebug);
                        _tmpDebug.sendTo(_player);
                    }
                }
                else
                {
                    _activeArea.addHole(_holePoints);
                    _holePoints.clear();
                    _holePoints = null;
                    run();
                    _tmpDebug.reset();
                    _tmpDebug.sendTo(_player);
                    _tmpDebug = null;
                    _activeArea.updateSpawns();
                    SpawnAreasTable.getInstance().saveSpawnArea(_activeArea);
                    _mode = MODE_IDLE;
                }
                return true;
            }



            return false;
        }


        @Override
        public void run()
        {
            hideAll();
            List<SpawnArea> areas = SpawnAreasTable.getInstance().getAreasNearPlayer(_player);
            for (SpawnArea area : areas) {
                if (_activeArea == null || _activeArea == area) {
                    ExServerPrimitive debug = _debugs.get(area);
                    if (debug == null) {
                        debug = _player.getDebugPacket("SA_" + area.getId());
                        area.showDebug(debug, area == _activeArea);
                        if (_activeLocation != null) {
                            area.renderPoint(debug, Color.MAGENTA, _activeLocation);
                            area.renderStraightLine(debug, Color.MAGENTA, _player.getPosition(), _activeLocation);
                            Location dst = new Location(_activeLocation);
                            dst.setZ(_player.getPosition().getZ() - 500);
                            area.renderStraightLine(debug, Color.MAGENTA, _activeLocation, dst);
                        }
                        _debugs.put(area, debug);
                        debug.sendTo(_player);
                    }
                }
            }

            showMainPage(_player, 1);
        }
    }
}
