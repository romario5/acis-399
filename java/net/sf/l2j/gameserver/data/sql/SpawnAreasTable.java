package net.sf.l2j.gameserver.data.sql;

import net.sf.l2j.Config;
import net.sf.l2j.commons.logging.CLogger;
import net.sf.l2j.commons.pool.ConnectionPool;
import net.sf.l2j.gameserver.data.xml.NpcData;
import net.sf.l2j.gameserver.geoengine.GeoEngine;
import net.sf.l2j.gameserver.model.World;
import net.sf.l2j.gameserver.model.WorldRegion;
import net.sf.l2j.gameserver.model.actor.Player;
import net.sf.l2j.gameserver.model.actor.template.NpcTemplate;
import net.sf.l2j.gameserver.model.location.Location;
import net.sf.l2j.gameserver.model.location.SpawnLocation;
import net.sf.l2j.gameserver.model.spawn.Spawn;
import net.sf.l2j.gameserver.model.spawn.SpawnArea;
import net.sf.l2j.gameserver.model.spawn.SpawnGroup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpawnAreasTable
{
    private static final CLogger LOGGER = new CLogger(SpawnAreasTable.class.getName());

    private static final String LOAD_AREAS = "SELECT * FROM spawn_areas";
    private static final String LOAD_GROUPS = "SELECT * FROM spawn_areas_groups";
    private static final String LOAD_NPC = "SELECT * FROM spawn_areas_npc";
    private static final String UPDATE_AREA = "UPDATE spawn_areas SET points = ? WHERE id = ?";
//    private static final String ADD_SPAWN = "INSERT INTO spawnlist (npc_templateid,locx,locy,locz,heading,respawn_delay) values(?,?,?,?,?,?)";
//    private static final String DELETE_SPAWN = "DELETE FROM spawnlist WHERE locx=? AND locy=? AND locz=? AND npc_templateid=? AND heading=?";

    private final Map<String, SpawnArea> _areas = new HashMap<>();
    private final Map<String, SpawnGroup> _groups = new HashMap<>();
    private final Map<String, Spawn> _spawnsByTag = new HashMap<>();
    private final Map<String, SpawnGroup> _groupsByTag = new HashMap<>();
    private final List<Spawn> _nightSpawns = new ArrayList<>();
    private final List<Spawn> _daySpawns = new ArrayList<>();
    private final Map<WorldRegion, List<SpawnArea>> _areasByRegion = new HashMap<>();

    protected SpawnAreasTable()
    {
        if (!Config.NO_SPAWNS) {
            loadAreas();
            loadGroups();
            loadNpc();

            for (Map.Entry<String, SpawnGroup> entry : _groups.entrySet()) {
                entry.getValue().doSpawn();
            }
        }
    }

    private void loadAreas()
    {
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement ps = con.prepareStatement(LOAD_AREAS);
             ResultSet rs = ps.executeQuery())
        {

            while (rs.next()) {
                String pos = rs.getString("points");

                String[] d = pos.split("\\|");
                if (d.length == 0) continue;

                List<Location> points = parsePoints(d[0]);
                if (points.size() < 3) continue;
                List<List<Location>> holes = new ArrayList<>();

                if (d.length > 1) {
                    for (int i = 1; i < d.length; i++) {
                        List<Location> h = parsePoints(d[i]);
                        if (h.size() >= 3) {
                            holes.add(h);
                        }
                    }
                }

                SpawnArea area;
                String areaId = rs.getString("id");
                int minZ = rs.getInt("min_z");
                int maxZ = rs.getInt("max_z");
                try {
                    area = new SpawnArea(areaId, points, minZ, maxZ);
                    for (List<Location> h : holes) {
                        area.addHole(h);
                    }
                    area.precalculateRandomDistribution();
                } catch (Exception e) {
                    LOGGER.error("Error on creating spawn area " + areaId + ": ", e);
                    continue;
                }

                _areas.put(area.getId(), area);

                WorldRegion region = World.getInstance().getRegion(area.getCenter());
                if (region != null) {
                    List<SpawnArea> regionAreas = _areasByRegion.computeIfAbsent(region, k -> new ArrayList<>());
                    regionAreas.add(area);
                } else {
                    LOGGER.warn("Unable to find region for area " + area.getId());
                }
            }
        } catch (Exception e)
        {
            LOGGER.error("Couldn't load spawn areas.", e);
        }
        LOGGER.info("Loaded {} spawn areas.", _areas.size());
    }

    private List<Location> parsePoints(String data)
    {
        String[] arr = data.split(";");
        ArrayList<Location> points = new ArrayList<>();
        Location loc = new Location(0, 0, 0);
        int i = 0;
        for (String a : arr) {
            if (i == 0) {
                loc.setX(Integer.parseInt(a));
            } else if (i == 1) {
                loc.setY(Integer.parseInt(a));
            } else if (i == 2) {
                loc.setZ(Integer.parseInt(a));
                loc.setZ(GeoEngine.getInstance().getHeight(loc));
                points.add(loc.clone());
                i = 0;
                continue;
            }
            i++;
        }
        return points;
    }


    public SpawnArea getArea(String id)
    {
        return _areas.get(id);
    }

    public List<SpawnArea> getAreasByRegion(WorldRegion region)
    {
        return _areasByRegion.get(region);
    }

    public List<SpawnArea> getAreasNearPlayer(Player player)
    {
        List<SpawnArea> areas = new ArrayList<>();
        WorldRegion region = player.getRegion();
        if (region != null) {
            List<SpawnArea> a = SpawnAreasTable.getInstance().getAreasByRegion(region);
            if (a != null) {
                for (SpawnArea area : a) {
                    if (!areas.contains(area)) areas.add(area);
                }
            }
            for (WorldRegion r : region.getSurroundingRegions()) {
                a = SpawnAreasTable.getInstance().getAreasByRegion(r);
                if (a != null) {
                    for (SpawnArea area : a) {
                        if (!areas.contains(area)) areas.add(area);
                    }
                }
            }
        }
        return areas;
    }


    private void loadGroups()
    {
        int totalGroups = 0;
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement ps = con.prepareStatement(LOAD_GROUPS);
             ResultSet rs = ps.executeQuery())
        {
            while (rs.next()) {
                String groupId = rs.getString("id");
                String spawnAreasId = rs.getString("spawn_areas_id");
                int maxNpcCount = rs.getInt("max_npc_count");

                String[] arr = spawnAreasId.split(";");
                SpawnGroup group = new SpawnGroup(groupId);
                group.setMaxNpcCount(maxNpcCount);
                for (String areaId : arr) {
                    SpawnArea area = _areas.get(areaId);
                    if (area != null) {
                        group.addSpawnArea(area);
                    }
                }
                _groups.put(group.getId(), group);
                totalGroups++;
            }
        } catch (Exception e)
        {
                LOGGER.error("Couldn't load spawn areas.", e);
        }
        LOGGER.info("Loaded {} spawn areas groups.", totalGroups);
    }


    private void loadNpc()
    {
        int totalSpawns = 0;
        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement ps = con.prepareStatement(LOAD_NPC);
             ResultSet rs = ps.executeQuery())
        {
            boolean tagIsEmpty;
            while (rs.next()) {
                String groupId = rs.getString("spawn_group_id");
                int npcTemplateId = rs.getInt("npc_template_id");
                SpawnGroup group = _groups.get(groupId);
                if (group == null) {
                    LOGGER.error("Group " + groupId + " is missing for NPC " + npcTemplateId);
                    continue;
                }

                NpcTemplate npcTemplate = NpcData.getInstance().getTemplate(npcTemplateId);
                if (npcTemplate == null) {
                    LOGGER.error("NPC template is missing (" + npcTemplateId + ")");
                    continue;
                }

                int count = rs.getInt("count");


                String pos = rs.getString("position");
                ArrayList<SpawnLocation> points = new ArrayList<>();
                ArrayList<Integer> chances = new ArrayList<>();

                if (pos != null && !pos.equals("anywhere") && !pos.isEmpty()) {
                    try {
                        String[] arr = pos.split("\\|");
                        for (String a : arr) {
                            SpawnLocation loc = new SpawnLocation(0, 0, 0, 0);
                            String[] arr2 = a.split(";");
                            int i = 0;
                            for (String a2 : arr2) {
                                if (i == 0) {
                                    loc.setX(Integer.parseInt(a2));
                                } else if (i == 1) {
                                    loc.setY(Integer.parseInt(a2));
                                } else if (i == 2) {
                                    loc.setZ(Integer.parseInt(a2));
                                } else if (i == 3) {
                                    loc.setHeading(Integer.parseInt(a2));
                                } else if (i == 4) {
                                    points.add(loc);
                                    chances.add(Integer.parseInt(a2));
                                    i = 0;
                                    continue;
                                }
                                i++;
                            }
                        }
                    } catch (Exception e) {
                        LOGGER.error("Error on parsing position (" + groupId + ")", e);
                    }
                }


                boolean doSpawn = true;
                for (int i = 0; i < count; i++) {
                    Spawn spawn = new Spawn(npcTemplate);

                    for (int j = 0; j < points.size(); j++) {
                        spawn.addSpot(points.get(j), chances.get(j));
                    }

                    String tag = rs.getString("tag");
                    tagIsEmpty = tag == null || tag.isEmpty();
                    if (!tagIsEmpty) {
                        _groupsByTag.put(tag, group);
                        _spawnsByTag.put(tag, spawn);
                    }

                    spawn.setRespawnDelay(rs.getInt("respawn_delay"));
                    spawn.setRespawnRandom(rs.getInt("respawn_delay_random"));
                    spawn.setRespawnState(true);

                    String timeOfDay = rs.getString("time_of_day");
                    if (timeOfDay != null) {
                        if (timeOfDay.equals("day")) {
                            spawn.setTimeOfDay(Spawn.TIME_DAY);
                        } else  if (timeOfDay.equals("night")) {
                            spawn.setTimeOfDay(Spawn.TIME_NIGHT);
                        }
                    }

                    group.addSpawn(spawn);

                    totalSpawns++;
                }
            }
        } catch (Exception e)
        {
            LOGGER.error("Couldn't load spawn areas.", e);
        }
        LOGGER.info("Loaded {} spawn areas NPCs.", totalSpawns);
    }


    public boolean saveSpawnArea(SpawnArea area)
    {

        try (Connection con = ConnectionPool.getConnection();
             PreparedStatement ps = con.prepareStatement(UPDATE_AREA))
        {
            StringBuilder sb = new StringBuilder();
            ps.setString(1, area.toString());
            ps.setString(2, area.getId());
            ps.execute();
        } catch (Exception e)
        {
            LOGGER.error("Couldn't save spawn area " + area.getId() + ".", e);
            return false;
        }
        return true;
    }


    public static SpawnAreasTable getInstance()
    {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder
    {
        protected static final SpawnAreasTable INSTANCE = new SpawnAreasTable();
    }
}
